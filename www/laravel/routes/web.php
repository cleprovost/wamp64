<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DescController@index');
Route::get('/mammiferes', 'MammController@index');
Route::get('/reptiles', 'ReptController@index');
Route::get('/birds', 'BirdController@index');
Route::get('/animals', 'AnimController@index');

//Route::resource('posts', 'PostsController');
Route::resource('animals', 'AnimController');
Route::resource('reptiles', 'ReptController');
Route::resource('mammiferes', 'MammController');
Route::resource('birds', 'BirdController');

