<?php

namespace App\Http\Controllers;

use App\Bird;
use App\Mammifere;
use App\Reptile;
use Dotenv\Validator;
use Illuminate\Http\Request;
use App\Animal;

class AnimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $animals = Animal::orderBy('created_at', 'desc')->paginate(10);
        return view('Animals.index')->with('animals', $animals);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Animals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'type' => 'required'
        ]);

        $animal = new Animal;
        $animal->name = $request->input('name');
        $animal->type = $request->input('type');
        $animal->save();

        if($request->input('type') == 'reptile'){
            $reptile = new Reptile;
            $reptile->id = $animal->id;
            $reptile->name = $request->input('name');
            $reptile->scale = 'Veuillez indiquer mes écailles';
            $reptile->save();
        }

        if($request->input('type') == 'mammifere'){
            $mammifere = new Mammifere;
            $mammifere->id = $animal->id;
            $mammifere->name = $request->input('name');
            $mammifere->fur = 'Veuillez indiquer mon pelage';
            $mammifere->save();
        }

        if($request->input('type') == 'oiseau'){
            $bird = new Bird;
            $bird->id = $animal->id;
            $bird->name = $request->input('name');
            $bird->feathers = 'Veuillez indiquer mon plumage';
            $bird->save();
        }

        return redirect('/animals')->with('success', 'Votre animal à été ajouté');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $animal = Animal::find($id);
        return view('Animals.show')->with('animal', $animal);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $animal = Animal::find($id);
        return view('Animals.edit')->with('animal', $animal);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'type' => 'required'
        ]);

        $animal = Animal::find($id);
        $animal->name = $request->input('name');
        $animal->type = $request->input('type');
        $animal->save();

        if($request->input('type') == 'reptile'){
            $reptile = Reptile::find($id);
            $reptile->name = $request->input('name');
            $reptile->scale = 'Veuillez indiquer mes écailles';
            $reptile->save();
        }

        if($request->input('type') == 'mammifere'){
            $mammifere = Mammifere::find($id);
            $mammifere->name = $request->input('name');
            $mammifere->fur = 'Veuillez indiquer mon pelage';
            $mammifere->save();
        }

        if($request->input('type') == 'oiseau'){
            $bird = Bird::find($id);
            $bird->id = $animal->id;
            $bird->name = $request->input('name');
            $bird->feathers = 'Veuillez indiquer mon plumage';
            $bird->save();
        }

        return redirect('/animals')->with('success', 'Votre animal à été modifié');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $animal = Animal::find($id);
        $animal -> delete();
        if($animal->type == "reptile"){
            $animal = Reptile::find($id);
            $animal -> delete();
        }
        if($animal->type == "mammifere"){
            $animal = Mammifere::find($id);
            $animal -> delete();
        }
        if($animal->type == "oiseau"){
            $animal = Bird::find($id);
            $animal -> delete();
        }
        return redirect('/animals')->with('success', 'Votre animal à été supprimé');

    }

    public static function toString($id){
        $animal = Animal::find($id);
        return 'je suis un(e) '.$animal->name;
    }

    public static function peau($id){
        $animal = Animal::find($id);
        if($animal->type == "reptile"){
            $animal = Reptile::find($id);
            return 'mes écailles sont '.$animal->scale;
        }
        if($animal->type == "mammifere"){
            $animal = Mammifere::find($id);
            return 'ma fourrure est '.$animal->fur;
        }
        if($animal->type == "oiseau"){
            $animal = Bird::find($id);
            return 'mon plumage est '.$animal->feathers;
        }

    }

}
