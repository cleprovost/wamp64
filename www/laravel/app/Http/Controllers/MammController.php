<?php

namespace App\Http\Controllers;

use App\Animal;
use Illuminate\Http\Request;
use App\Mammifere;

class MammController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mammiferes = Mammifere::orderBy('created_at', 'desc')->paginate(10);
        return view('Mammiferes.index')->with('mammiferes', $mammiferes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Mammiferes.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'fur' => 'required'
        ]);

        $Animal = new Animal;
        $Animal->name = $request->input('name');
        $Animal->type = "mammifere";
        $Animal->save();

        $Mammifere = new Mammifere;
        $Mammifere->id = $Animal->id;
        $Mammifere->name = $request->input('name');
        $Mammifere->fur = $request->input('fur');
        $Mammifere->save();

        return redirect('/mammiferes')->with('success', 'votre animal à été ajouté');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mammifere = Mammifere::find($id);
        return view('Mammiferes.show')->with('mammifere', $mammifere);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mammifere = Mammifere::find($id);
        return view('Mammiferes.edit')->with('mammifere', $mammifere);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'fur' => 'required'
        ]);

        $Mammifere = Mammifere::find($id);
        $Mammifere->name = $request->input('name');
        $Mammifere->fur = $request->input('fur');
        $Mammifere->save();

        $Animal = Animal::find($id);
        $Animal->name = $request->input('name');
        $Animal->type = "mammifere";
        $Animal->save();

        return redirect('/mammiferes')->with('success', 'Votre animal à été modifié');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Mammifere = Mammifere::find($id);
        $Mammifere -> delete();
        $animal = Animal::find($id);
        $animal->delete();
        return redirect('/mammiferes')->with('success', 'Votre animal à été supprimé');

    }

    public static function growl($id){
        $mammifere = Mammifere::find($id);
        return 'je suis un(e) '.$mammifere->name;
    }

    public static function fur($id){
        $Mammifere = Mammifere::find($id);
        return 'ma fourrure est '.$Mammifere->fur;
    }
}
