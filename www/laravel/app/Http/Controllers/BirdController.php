<?php

namespace App\Http\Controllers;

use App\Animal;
use Illuminate\Http\Request;
use App\Bird;

class BirdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $birds = Bird::orderBy('created_at', 'desc')->paginate(10);
        return view('Birds.index')->with('birds', $birds);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Birds.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'feathers' => 'required'
        ]);

        $Animal = new Animal;
        $Animal->name = $request->input('name');
        $Animal->type = "oiseau";
        $Animal->save();

        $bird = new Bird;
        $bird->id = $Animal->id;
        $bird->name = $request->input('name');
        $bird->feathers = $request->input('feathers');
        $bird->save();

        return redirect('/birds')->with('success', 'your animal has been added');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bird = Bird::find($id);
        return view('Birds.show')->with('bird', $bird);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bird = Bird::find($id);
        return view('Birds.edit')->with('bird', $bird);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'type' => '',
            'feathers' => 'required'
        ]);

        $bird = Bird::find($id);
        $bird->name = $request->input('name');
        $bird->feathers = $request->input('feathers');
        $bird->save();

        $Animal = Animal::find($id);
        $Animal->name = $request->input('name');
        $Animal->type = 'oiseau';
        $Animal->save();

        return redirect('/birds')->with('success', 'Your animal has been modificated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bird = Bird::find($id);
        $bird -> delete();
        $animal = Animal::find($id);
        $animal->delete();
        return redirect('/birds')->with('success', 'Your animal has been deleted');

    }

    public static function tweet($id){
        $bird = Bird::find($id);
        return 'je suis un(e) '.$bird->name;
    }

    public static function feather($id){
        $bird = Bird::find($id);
        return 'mon plumage est '.$bird->feathers;
    }

}
