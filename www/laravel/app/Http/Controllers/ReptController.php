<?php

namespace App\Http\Controllers;

use App\Animal;
use App\Reptile;
use Illuminate\Http\Request;

class ReptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reptiles = Reptile::orderBy('created_at', 'desc')->paginate(10);
        return view('Reptiles.index')->with('reptiles', $reptiles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Reptiles.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'scale' => 'required'
        ]);

        $Animal = new Animal;
        $Animal->name = $request->input('name');
        $Animal->type = "reptile";
        $Animal->save();

        $Reptile = new Reptile;
        $Reptile->id = $Animal->id;
        $Reptile->name = $request->input('name');
        $Reptile->scale = $request->input('scale');
        $Reptile->save();

        return redirect('/reptiles')->with('success', 'Votre aniaml à été ajouté');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reptile = Reptile::find($id);
        return view('Reptiles.show')->with('reptile', $reptile);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reptile = Reptile::find($id);
        return view('Reptiles.edit')->with('reptile', $reptile);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'scale' => 'required'
        ]);

        $Reptile = Reptile::find($id);
        $Reptile->name = $request->input('name');
        $Reptile->scale = $request->input('scale');
        $Reptile->save();

        $Animal = Animal::find($id);
        $Animal->name = $request->input('name');
        $Animal->type = "reptile";
        $Animal->save();

        return redirect('/reptiles')->with('success', 'Votre animal à été modifié');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Reptile = Reptile::find($id);
        $Reptile -> delete();
        $animal = Animal::find($id);
        $animal->delete();
        return redirect('/reptiles')->with('success', 'Votre aniaml à été supprimé');

    }

    public static function hiss($id){
        $reptile = Reptile::find($id);
        return 'je suis un(e) '.$reptile->name;
    }

    public static function scale($id){
        $Reptile = Reptile::find($id);
        return 'mes écailles sont '.$Reptile->scale;
    }
}
