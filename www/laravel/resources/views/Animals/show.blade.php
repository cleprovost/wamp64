@extends('layout.app')

@if($animal->type == 'reptile')
    <link rel="stylesheet" href="{{ asset('css/reptile.css')}}">
@endif
@if($animal->type == 'oiseau')
    <link rel="stylesheet" href="{{ asset('css/oiseau.css')}}">
@endif
@if($animal->type == 'mammifere')
    <link rel="stylesheet" href="{{ asset('css/mammifere.css')}}">
@endif


@section('content')
    <div class="animal">
        <h1>{{$animal->name}}</h1>
        <small>ecrit le {{$animal->created_at}}</small>
        <div>
            {!! $animal->type !!}
        </div>
        <a href="/animals/{{$animal->id}}/edit" class="btn btn-lg btn-primary">Editer l'animal</a>

        {!! Form::open(['action' => ['AnimController@destroy', $animal->id], 'method' => 'animal']) !!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Supprimer', ['class' => 'btn btn-lg btn-danger'])}}
        {!! Form::close() !!}

    </div>

@endsection