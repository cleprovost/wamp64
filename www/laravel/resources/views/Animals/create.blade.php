@extends('layout.app')

@section('content')
    <h1>Création d'animaux</h1>
    {!! Form::open(['action' => 'AnimController@store', 'method' => 'POST']) !!}
        <div class="form-group">
            {{Form::label('name', 'Nom')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Titre'])}}
        </div>
        <div class="form-group">
            {{Form::label('type','espèces')}}
            {{Form::radio('type', 'reptile', true)}} reptile
            {{Form::radio('type', 'mammifere', false )}} mammifere
            {{Form::radio('type', 'oiseau', false) }} oiseau
        </div>
        {{Form::submit('Ajouter votre Animal', ['class' => 'btn btn-lg btn-primar'])}}
    {!! Form::close() !!}
    @endsection