@extends('layout.app')

@section('content')
    <h1>Animaux</h1>
    <hr>
    <a href="/animals/create" class="btn btn-lg btn-primary" style="margin-bottom:15px;">Creer un nouvel Animal</a>
    @if(count($animals) >= 1)
        @foreach($animals as $animal)
            <div class="animal">
                @if($animal->type == 'reptile')
                    <div class="well" style="background-color: #2ab27b; color: #ffffff;  ">
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        <h3><a href="/animals/{{$animal->id}}" style="color: black">{{\App\Http\Controllers\AnimController::toString($animal->id)}}</a></h3>
                                        <small>ecrit le {{$animal->created_at}}</small>
                                        <p>{{$animal->type}}</p>
                                        <p>{{\App\Http\Controllers\AnimController::peau($animal->id)}}</p>
                                    </td>
                                    <td style="padding: 0px 250px 0px 250px ">
                                        <a href="/animals/{{$animal->id}}/edit" class="btn btn-lg btn-primary">Editer animal</a>
                                        {!! Form::open(['action' => ['AnimController@destroy', $animal->id], 'method' => 'animal']) !!}
                                        {{Form::hidden('_method', 'DELETE')}}
                                        {{Form::submit('Supprimer', ['class' => 'btn btn-lg btn-danger'])}}
                                    </td>

                                </tr>
                            </tbody>
                        </table>

                        <hr>
                        <p><a href="/animals/{{$animal->id}}" style="color: black">Lire la suite</a></p>

                    </div>
                @endif
                @if($animal->type == 'mammifere')
                    <div class="well" style="background-color: #f7e1b5 ">
                        <table>
                            <tbody>
                            <tr>
                                <td>
                                    <h3><a href="/animals/{{$animal->id}}" style="color: black">{{\App\Http\Controllers\AnimController::toString($animal->id)}}</a></h3>
                                    <small>ecrit le {{$animal->created_at}}</small>
                                    <p>{{$animal->type}}</p>
                                    <p>{{\App\Http\Controllers\AnimController::peau($animal->id)}}</p>

                                </td>
                                <td style="padding: 0px 250px 0px 250px ">
                                    <a href="/animals/{{$animal->id}}/edit" class="btn btn-lg btn-primary">Editer animal</a>
                                    {!! Form::open(['action' => ['AnimController@destroy', $animal->id], 'method' => 'animal']) !!}
                                    {{Form::hidden('_method', 'DELETE')}}
                                    {{Form::submit('Supprimer', ['class' => 'btn btn-lg btn-danger'])}}
                                </td>

                            </tr>
                            </tbody>
                        </table>
                        <hr>
                        <p><a href="/animals/{{$animal->id}}">Lire la suite</a></p>

                    </div>
                @endif
                @if($animal->type == 'oiseau')
                    <div class="well" style="background-color: #777777; color: #ffffff">
                        <table>
                            <tbody>
                            <tr>
                                <td style="color: white">
                                    <h3><a href="/animals/{{$animal->id}}" style="color: white"; margin="auto">{{\App\Http\Controllers\AnimController::toString($animal->id)}}</a></h3>
                                    <small>ecrit le {{$animal->created_at}}</small>
                                    <p>{{$animal->type}}</p>
                                    <p>{{\App\Http\Controllers\AnimController::peau($animal->id)}}</p>
                                </td>
                                <td style="padding: 0px 250px 0px 250px; display: inline-block; float: left; ">
                                    <a href="/animals/{{$animal->id}}/edit" class="btn btn-lg btn-primary">Editer animal</a>
                                    {!! Form::open(['action' => ['AnimController@destroy', $animal->id], 'method' => 'animal']) !!}
                                    {{Form::hidden('_method', 'DELETE')}}
                                    {{Form::submit('Supprimer', ['class' => 'btn btn-lg btn-danger'])}}
                                </td>

                            </tr>
                            </tbody>
                        </table>
                        <hr>
                        <p><a href="/animals/{{$animal->id}}" style="color: white">Lire la suite</a></p>

                    </div>
                 @endif

            </div>
        @endforeach
        <a href="/animals/create" class="btn btn-lg btn-primary" style="margin-bottom:15px;">Creer un nouvel Animal</a>
    @else
        <p>Aucun animal existant !</p>
    @endif

@endsection