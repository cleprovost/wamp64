@extends('layout.app')

@section('content')
    <h1>Edition de l'Animal</h1>
    {!! Form::open(['action' => ['AnimController@update', $animal->id], 'method' => 'Animal']) !!}
    <div class="form-group">
        {{Form::label('name', 'Nom')}}
        {{Form::text('name', $animal->name, ['class' => 'form-control', 'placeholder' => 'le nom de l'animal'])}}
    </div>
    <div class="form-group">
        {{Form::label('type', 'Espece')}}
        {{Form::text('type', $animal->type, ['class' => 'form-control', 'placeholder' => 'l'espèce de l'animal'])}}
    </div>
    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Editer', ['class' => 'btn btn-lg btn-primary'])}}
@endsection

