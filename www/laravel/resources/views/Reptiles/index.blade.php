@extends('layout.app')

@section('content')
    <h1>Reptiles</h1>
    <hr>
    <a href="/reptiles/create" class="btn btn-lg btn-primary" style="margin-bottom:15px;">Creer un nouveau Reptile</a>
    @if(count($reptiles) >= 1)
        @foreach($reptiles as $reptile)
            <div class="well" style="background-color: #2ab27b; color: #ffffff;">
                <h3><a href="/reptiles/{{$reptile->id}}" style="color: black">{{\App\Http\Controllers\ReptController::hiss($reptile->id)}} et {{\App\Http\Controllers\ReptController::scale($reptile->id)}}</a></h3>
                <small>ecrit le {{$reptile->created_at}}</small>
                <div>
                    <a href="/reptiles/{{$reptile->id}}/edit" class="btn btn-lg btn-primary">Editer le reptile</a>
                    {!! Form::open(['action' => ['ReptController@destroy', $reptile->id], 'method' => 'reptile']) !!}
                    {{Form::hidden('_method', 'DELETE')}}
                    {{Form::submit('Supprimer', ['class' => 'btn btn-lg btn-danger'])}}
                </div>
                <hr>
                <p><a href="/reptiles/{{$reptile->id}}" style="color: black">Lire la suite</a></p>
            </div>

        @endforeach
        <a href="/reptiles/create" class="btn btn-lg btn-primary" style="margin-bottom:15px;">Creer un nouveau reptile</a>

    @else
        <p>Aucun Reptile existant !</p>
    @endif

@endsection