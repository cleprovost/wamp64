@extends('layout.app')

@section('content')
    <h1>Création de Reptiles</h1>
    {!! Form::open(['action' => 'ReptController@store', 'method' => 'POST']) !!}
        <div class="form-group">
            {{Form::label('name', 'Nom')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Nom'])}}
        </div>
        <div class="form-group">
            {{Form::label('scale','Écailles')}}
            {{Form::text('scale', '', ['class' => 'form-control', 'placeholder' => 'couleur des écailles'])}}
        </div>
        {{Form::submit('Ajouter votre Reptile', ['class' => 'btn btn-lg btn-primar'])}}
    {!! Form::close() !!}
    @endsection