@extends('layout.app')

<link rel="stylesheet" href="{{ asset('css/reptile.css')}}">

@section('content')
    <div class="animal">
        <h1>{{\App\Http\Controllers\ReptController::hiss($reptile->id)}}</h1>
        <small>ecrit le {{$reptile->created_at}}</small>
        <div>
            {{\App\Http\Controllers\ReptController::scale($reptile->id)}}
        </div>
        <a href="/reptiles/{{$reptile->id}}/edit" class="btn btn-lg btn-primary">Editer le reptile</a>

        {!! Form::open(['action' => ['ReptController@destroy', $reptile->id], 'method' => 'reptile']) !!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Supprimer', ['class' => 'btn btn-lg btn-danger'])}}
        {!! Form::close() !!}
    </div>

@endsection