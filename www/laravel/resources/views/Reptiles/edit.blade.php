@extends('layout.app')

@section('content')
    <h1>Edition du reptile</h1>
    {!! Form::open(['action' => ['ReptController@update', $reptile->id], 'method' => 'Animal']) !!}
    <div class="form-group">
        {{Form::label('name', 'Nom')}}
        {{Form::text('name', $reptile->name, ['class' => 'form-control', 'placeholder' => 'le nom du reptile'])}}
    </div>
    <div class="form-group">
        {{Form::label('scale', 'Écailles')}}
        {{Form::text('scale', $reptile->scale, ['class' => 'form-control', 'placeholder' => 'les écailles du reptile'])}}
    </div>
    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Editer votre Reptile', ['class' => 'btn btn-lg btn-primary'])}}
@endsection

