@extends('layout.app')

<link rel="stylesheet" href="{{ asset('css/oiseau.css')}}">

@section('content')
    <div class="animal">
        <h1>{{\App\Http\Controllers\BirdController::tweet($bird->id)}}</h1>
        <small>ecrit le {{$bird->created_at}}</small>
        <div>
            {{\App\Http\Controllers\BirdController::feather($bird->id)}}
        </div>
        <a href="/birds/{{$bird->id}}/edit" class="btn btn-lg btn-primary">Editer l'article</a>

        {!! Form::open(['action' => ['BirdController@destroy', $bird->id], 'method' => 'bird']) !!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Supprimer', ['class' => 'btn btn-lg btn-danger'])}}
        {!! Form::close() !!}
    </div>

@endsection