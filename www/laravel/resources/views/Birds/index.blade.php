@extends('layout.app')

@section('content')
    <h1>Oiseaux</h1>
    <hr>
    <a href="/birds/create" class="btn btn-lg btn-primary" style="margin-bottom:15px;">Creer un nouvel oiseau</a>
    @if(count($birds) >= 1)
        @foreach($birds as $bird)
            <div class="well" style="background-color: #777777; color: #ffffff">
                <h3><a href="/birds/{{$bird->id}}">{{\App\Http\Controllers\BirdController::tweet($bird->id)}} et {{\App\Http\Controllers\BirdController::feather($bird->id)}}</a></h3>
                <small>ecrit le {{$bird->created_at}}</small>
                <hr>
                <div>
                    <a href="/birds/{{$bird->id}}/edit" class="btn btn-lg btn-primary">Editer l'oiseau</a>
                    {!! Form::open(['action' => ['BirdController@destroy', $bird->id], 'method' => 'bird']) !!}
                    {{Form::hidden('_method', 'DELETE')}}
                    {{Form::submit('Supprimer', ['class' => 'btn btn-lg btn-danger'])}}
                </div>
                <p><a href="/birds/{{$bird->id}}">Lire la suite</a></p>
            </div>

        @endforeach
        <a href="/birds/create" class="btn btn-lg btn-primary" style="margin-bottom:15px;">Creer un nouvel oiseau</a>

    @else
        <p>Aucun oiseau existant !</p>
    @endif

@endsection