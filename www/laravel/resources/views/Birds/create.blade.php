@extends('layout.app')

@section('content')
    <h1>Création d'oiseaux</h1>
    {!! Form::open(['action' => 'BirdController@store', 'method' => 'POST']) !!}
        <div class="form-group">
            {{Form::label('name', 'Nom')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Nom'])}}
        </div>
        <div class="form-group">
            {{Form::label('feathers','Plumage')}}
            {{Form::text('feathers', '', ['class' => 'form-control', 'placeholder' => 'couleur du plumage'])}}
        </div>
        {{Form::submit('Ajouter votre oiseau', ['class' => 'btn btn-lg btn-primar'])}}
    {!! Form::close() !!}
    @endsection