@extends('layout.app')

@section('content')
    <h1>Edition pour les oiseaux</h1>
    {!! Form::open(['action' => ['BirdController@update', $bird->id], 'method' => 'Bird']) !!}
    <div class="form-group">
        {{ Form::label('name', 'Nom') }}
        {{ Form::text('name', $bird->name, ['class' => 'form-control', 'placeholder' => "le nom de l'oiseau"]) }}
    </div>
    <div class="form-group">
        {{ Form::label('feathers', 'Plumage') }}
        {{ Form::text('feathers', $bird->feathers, ['class' => 'form-control', 'placeholder' => 'son plumage :']) }}
    </div>
    {{ Form::hidden('_method', 'PUT') }}
    {{ Form::submit('Editer', ['class' => 'btn btn-lg btn-primary']) }}
@endsection

