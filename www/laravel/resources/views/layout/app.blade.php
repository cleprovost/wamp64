<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1">
    <link rel="stylesheet" href="{{asset('css/app.css')}}" media="all">
    <title>{{config('app.name', 'Application Laravel')}}</title>
</head>
<body>
    @include('inc.navbar')
    <div class="container">
        @include('inc.errorsuccess')
        @yield('content')
    </div>
</body>
</html>