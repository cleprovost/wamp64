@extends('layout.app')

<link rel="stylesheet" href="{{ asset('css/mammifere.css')}}">

@section('content')
    <div class="animal">
        <h1>{{\App\Http\Controllers\MammController::growl($mammifere->id)}}</h1>
        <small>ecrit le {{$mammifere->created_at}}</small>
        <div>
            {{\App\Http\Controllers\MammController::fur($mammifere->id)}}
        </div>
        <a href="/mammiferes/{{$mammifere->id}}/edit" class="btn btn-lg btn-primary">Editer l'article</a>

        {!! Form::open(['action' => ['MammController@destroy', $mammifere->id], 'method' => 'mammifere']) !!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Supprimer', ['class' => 'btn btn-lg btn-danger'])}}
        {!! Form::close() !!}
    </div>

@endsection