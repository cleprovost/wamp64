@extends('layout.app')

@section('content')
    <h1>Création de mammifere</h1>
    {!! Form::open(['action' => 'MammController@store', 'method' => 'POST']) !!}
        <div class="form-group">
            {{Form::label('name', 'Nom')}}
            {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Nom'])}}
        </div>
        <div class="form-group">
            {{Form::label('fur','Fourrure')}}
            {{Form::text('fur', '', ['class' => 'form-control', 'placeholder' => 'couleur de la fourrure'])}}
        </div>
        {{Form::submit('Ajouter votre mammifere', ['class' => 'btn btn-lg btn-primar'])}}
    {!! Form::close() !!}
    @endsection