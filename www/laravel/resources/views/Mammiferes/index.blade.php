@extends('layout.app')

@section('content')
    <h1>Mammifere</h1>
    <hr>
    <a href="/mammiferes/create" class="btn btn-lg btn-primary" style="margin-bottom:15px;">Creer un nouveau Mammifere</a>
    @if(count($mammiferes) >= 1)
        @foreach($mammiferes as $mammifere)
            <div class="well" style="background-color: #f7e1b5 ">
                <h3><a href="/mammiferes/{{$mammifere->id}}">{{\App\Http\Controllers\MammController::growl($mammifere->id)}} et {{\App\Http\Controllers\MammController::fur($mammifere->id)}}</a></h3>
                <small>ecrit le {{$mammifere->created_at}}</small>
                <div>
                    <a href="/mammiferes/{{$mammifere->id}}/edit" class="btn btn-lg btn-primary">Editer le mammiferes</a>
                    {!! Form::open(['action' => ['MammController@destroy', $mammifere->id], 'method' => 'mammifere']) !!}
                    {{Form::hidden('_method', 'DELETE')}}
                    {{Form::submit('Supprimer', ['class' => 'btn btn-lg btn-danger'])}}
                </div>
                <hr>
                <p><a href="/mammiferes/{{$mammifere->id}}">Lire la suite</a></p>
            </div>
        @endforeach
        <a href="/mammiferes/create" class="btn btn-lg btn-primary" style="margin-bottom:15px;">Creer un nouveau Mammifere</a>
    @else
        <p>Aucun Mammifere existant !</p>
    @endif

@endsection