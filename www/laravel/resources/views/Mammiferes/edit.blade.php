@extends('layout.app')

@section('content')
    <h1>Edition du mammifere</h1>
    {!! Form::open(['action' => ['MammController@update', $mammifere->id], 'method' => 'Animal']) !!}
    <div class="form-group">
        {{Form::label('name', 'Nom')}}
        {{Form::text('name', $mammifere->name, ['class' => 'form-control', 'placeholder' => 'le nom de votre mammifere'])}}
    </div>
    <div class="form-group">
        {{Form::label('fur', 'Fourrure')}}
        {{Form::text('fur', $mammifere->fur, ['class' => 'form-control', 'placeholder' => 'la fourrure de votre mammifere'])}}
    </div>
    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Modifie ton Mammifere', ['class' => 'btn btn-lg btn-primary'])}}
@endsection

