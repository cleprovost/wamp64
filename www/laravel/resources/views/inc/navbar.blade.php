<nav class="navbar navbar-inverse">
    <div class="container">
        <div class=navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target=".navbar-collapse" >
                <span class="sr-only">Afficher Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>

            </button>
            <a class="navbar-brand" href="/">Accueil</a>
        </div>


        <div class="collapse navbar-collapse">
            <ul class="navbar-nav nav">
                <li>
                    <a href="/animals">Animaux</a>
                </li>
                <li>
                    <a href="/reptiles">Reptiles</a>
                </li>
                <li>
                    <a href="/mammiferes">Mammiferes</a>
                </li>
                <li>
                    <a href="/birds">Oiseaux</a>
                </li>
            </ul>
        </div>
    </div>

</nav>