@extends('layout.app')

@section('content')
    <div class="jumbotron">
        <h2 class="display-4">Application Laravel</h2>
        <p class="lead"> Bienvenue</p>
        <ul class="navbar-nav nav">
            <li>
                <a href="/animals">Tous les Animaux</a>
            </li>
            <li>
                <a href="/reptiles">Reptiles</a>
            </li>
            <li>
                <a href="/mammiferes">Mammiferes</a>
            </li>
            <li>
                <a href="/birds">Oiseaux</a>
            </li>
        </ul>

    </div>

    @endsection